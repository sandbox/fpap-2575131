Workflow Hide Fields is an addon to the workflow module.
This module permits configure workflows in a way they will be constrained to:
- Once created, only users with a given ("supervisor") role will can edit it.
- The "supervisor" user will select a responsible (one of a list from a given field).
  The dropdown list used to select the responsible is only visible when "supervisor" is editing the node.
- Once a responsible is set, only he or she will can edit it, until the workflow reaches given "prefinal" states.
- Once the workflow reaches some "prefinal" state, only the author will can edit it, rating it and finishing it.
  The field used to rank is only visible when the author is finishing the workflow.
- Specific fields can be set to edit only on creation.

Work order Life cycle example

          Author          Supervisor                  Responsible
             |                 |                           |
             |  Creates a      |                           |
             |  Work order     |                           |
             |---------------->|                           |
            |||                |                           |
            |||                |  Accepts the work         |
            |||                |  order and assigns        |
            |||                |  it to a responsible      |
            |||                |-------------------------->|
            |||                                            |
            |||                                            |
            |||                                      Begins working
            |||                                            |
            |||                                           \./
            |||                                            |
            |||                                            |
            |||                                Finish      |
            |||                                the job     |
            |||<-------------------------------------------| 
             |
             |
           Ranks
          the job
    (end of life cycle)




Author
-------
Originally written by Fernando Martín Pap <fernandopap [at] hotmail.com>.