<?php

/**
 * @file
 * Workflow hide fields Administration functionality.
 */

/**
 * Tha ajax callback called when node type is changed.
 * 
 * @param array $form
 *    An associative array containing the structure of the form.
 * @param array $form_state
 *   A keyed array containing the current state of the form.
 * @return array
 *   The entire field section.
 */
function ajax_fields_callback($form, $form_state) {
  return $form['options'][0]['fields'];
}

/**
 * Tha ajax callback called when field state is changed.
 * 
 * @param array $form
 *    An associative array containing the structure of the form.
 * @param array $form_state
 *   A keyed array containing the current state of the form.
 * @return array
 *   The entire states section.
 */
function ajax_states_callback($form, $form_state) {
  return $form['options'][0]['states'];
}

/**
 * Gets the fields of a given node type.
 * 
 * @param string $nodetype
 *   The type of the node.
 * @return array
 *   Associative array of machine name and label of fields.
 */
function _ajax_get_fields_options($nodetype = '') {
  $fields = array();

  if (isset($nodetype)) {
    // Get the $nodetype fields
    $data = field_info_instances("node", $nodetype);

    foreach ($data as $key => $value) {
      $fields[$key] = $value['label'];
    }

    // If $nodetype has title, add it
    $types = node_type_get_types();

    if (isset($types[$nodetype]) && $types[$nodetype]->has_title) {
      $fields['title'] = $types[$nodetype]->title_label;
    }
  }

  return $fields;
}

/**
 * Gets the states of a workflow field
 * 
 * @param string $field
 *   The name of the workflow field.
 * @return array
 *   Associative array of number and description of states. 
 */
function _ajax_get_states($field) {
  $theFieldInformation = field_info_field($field);

  $statesArray = explode(PHP_EOL, $theFieldInformation['settings']['allowed_values_string']);

  $associativeStatesArray = array();
  foreach ($statesArray as $result) {
    $b = explode(' |', $result);
    if (isset($b[0]) && isset($b[1]))
      $associativeStatesArray[$b[0]] = $b[1];
  }

  return $associativeStatesArray;
}

/**
 * Given a list of fields, it returns only those who are of type 'workflow'.
 *
 * @param array $nodesTypesList
 *   Pass in a set of fields.
 * @return array
 *   Returns an array of fields of type 'workflow'.
 */
function _get_fields_of_workflow_type($nodesTypesList) {
  $fields_of_workflow_type = array();
  foreach ($nodesTypesList as $key => $value) {
    // For each field, get its information
    $theFieldInformation = field_info_field($key);
    // If the type of the field is 'workflow', add the field to the list of workflow
    if ($theFieldInformation['type'] == 'workflow') {
      $fields_of_workflow_type[$key] = $value;
    }
  }

  return $fields_of_workflow_type;
}

/**
 * Defines the form elements used to edit the Workflow hide fields options.
 *
 * @param array $options [optional]
 *   Pass in a set of default values for the options.
 * @return array
 *   Returns the option set array.
 */
function workflow_hide_fields_option_elements($options = array()) {
  $form = array();

  // General Settings
  $form['general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
  );

  // Get all node types
  $allNodeTypes = node_type_get_names();
  $workflowNodeTypes = array();

  // From all node types, get only those which gets a field of type 'workflow'
  foreach ($allNodeTypes as $nodeTypeKey => $nodeType) {
    // Get the $nodeType fields
    $data = field_info_instances("node", $nodeTypeKey);

    foreach ($data as $key => $value) {
      // For each field, get its information
      $theFieldInformation = field_info_field($key);
      // If the type of the field is 'workflow', add the node type to the list of workflow node types
      if ($theFieldInformation['type'] == 'workflow') {
        $workflowNodeTypes[$nodeTypeKey] = $nodeType;
        break;
      }
    }
  }

  $nodeTypes['_select_an_item'] = t('Please select an item');
  $nodeTypes = array_merge($nodeTypes, $workflowNodeTypes);
  $form['general_settings']['nodeType'] = array(
    '#type' => 'select',
    '#title' => t('Node type'),
    '#multiple' => FALSE,
    '#description' => t('The Node type'),
    '#options' => $nodeTypes,
    '#default_value' => $options['nodeType'],
    // Bind an ajax callback to the change event (which is the default for the
    // select form type) of the first dropdown. It will replace the 'fields' fieldset when rebuilt.
    '#ajax' => array(
      // When 'event' occurs, Drupal will perform an ajax request in the
      // background. Usually the default value is sufficient (eg. change for
      // select elements), but valid values include any jQuery event,
      // most notably 'mousedown', 'blur', and 'submit'.
      'event' => 'change',
      'callback' => 'ajax_fields_callback',
      'wrapper' => 'fields-replace',
    ),
  );

  // Fields Settings
  $form['general_settings']['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields'),
    // The entire enclosing div created here gets replaced when nodeType is changed.
    '#prefix' => '<div id="fields-replace">',
    '#suffix' => '</div>',
  );

  $form['general_settings']['fields']['fieldResponsible'] = array(
    '#type' => 'select',
    '#title' => t('Field Responsible'),
    '#description' => t('The Field Responsible'),
    '#default_value' => isset($options['fieldResponsible']) ? $options['fieldResponsible'] : '',
  );

  $form['general_settings']['fields']['fieldRating'] = array(
    '#type' => 'select',
    '#title' => t('Field Rating'),
    '#description' => t('The Field Rating'),
    '#default_value' => isset($options['fieldRating']) ? $options['fieldRating'] : '',
  );

  $form['general_settings']['fields']['fieldState'] = array(
    '#type' => 'select',
    '#title' => t('Field State'),
    '#description' => t('The Field State'),
    '#default_value' => isset($options['fieldState']) ? $options['fieldState'] : '',
    // Bind an ajax callback to the change event (which is the default for the
    // select form type) of tihs dropdown. It will replace the 'states' fieldset when rebuilt.
    '#ajax' => array(
      // When 'event' occurs, Drupal will perform an ajax request in the
      // background. Usually the default value is sufficient (eg. change for
      // select elements), but valid values include any jQuery event,
      // most notably 'mousedown', 'blur', and 'submit'.
      'event' => 'change',
      'callback' => 'ajax_states_callback',
      'wrapper' => 'states-replace',
    ),
  );

  $form['general_settings']['fields']['fieldsWritableOnlyOnCreation'] = array(
    '#type' => 'select',
    '#title' => t('Fields Writable Only On Creation'),
    '#description' => t('The Fields Writable Only On Creation'),
    '#default_value' => isset($options['fieldsWritableOnlyOnCreation']) ? $options['fieldsWritableOnlyOnCreation'] : '',
    '#multiple' => 'true',
  );


  // States Settings
  $form['general_settings']['states'] = array(
    '#type' => 'fieldset',
    '#title' => t('States'),
    // The entire enclosing div created here gets replaced when nodeType is changed.
    '#prefix' => '<div id="states-replace">',
    '#suffix' => '</div>',
  );

  $form['general_settings']['states']['stateResponsibleAssignment'] = array(
    '#type' => 'select',
    '#title' => t('State Responsible Assignment'),
    '#description' => t('The State Responsible Assignment'),
    '#default_value' => isset($options['stateResponsibleAssignment']) ? $options['stateResponsibleAssignment'] : '',
  );
  $form['general_settings']['states']['statesFinal'] = array(
    '#type' => 'select',
    '#title' => t('States Final'),
    '#description' => t('The States Final'),
    '#default_value' => isset($options['statesFinal']) ? $options['statesFinal'] : '',
    '#multiple' => 'true',
  );
  $form['general_settings']['states']['statesRating'] = array(
    '#type' => 'select',
    '#title' => t('States Rating'),
    '#description' => t('The States where the author rates the task'),
    '#default_value' => isset($options['statesRating']) ? $options['statesRating'] : '',
    '#multiple' => 'true',
  );
  $form['general_settings']['states']['statesResponsibleAssigned'] = array(
    '#type' => 'select',
    '#title' => t('States Responsible Assigned'),
    '#description' => t('The States Responsible Assigned'),
    '#default_value' => isset($options['statesResponsibleAssigned']) ? $options['statesResponsibleAssigned'] : '',
    '#multiple' => 'true',
  );
  $form['general_settings']['roleSupervisor'] = array(
    '#type' => 'select',
    '#title' => t('Role Supervisor'),
    '#description' => t('The Role Supervisor'),
    '#default_value' => isset($options['roleSupervisor']) ? $options['roleSupervisor'] : '',
    '#options' => user_roles(),
  );

  return $form;
}

/**
 * Form builder; Form to edit a given preset.
 * 
 * @param array $form
 *    An associative array containing the structure of the form.
 * @param array $form_state
 *   A keyed array containing the current state of the form.
 * @return array
 *   The form with all the preset fields.
 */
function workflow_hide_fields_form_preset_edit($form, &$form_state) {

  $theFieldInformation = field_info_field('field_flujo_de_trabajo');

  if (empty($form_state['preset'])) {
    $preset = workflow_hide_fields_preset_create();
  }
  else {
    $preset = $form_state['preset'];
  }

  // Title
  $form['title'] = array(
    '#type' => 'textfield',
    '#maxlength' => '255',
    '#title' => t('Title'),
    '#description' => t('A human-readable title for this option set.'),
    '#required' => TRUE,
    '#default_value' => $preset->title,
  );
  $form['name'] = array(
    '#type' => 'machine_name',
    '#maxlength' => '255',
    '#machine_name' => array(
      'source' => array('title'),
      'exists' => 'workflow_hide_fields_preset_exists',
    ),
    '#required' => TRUE,
    '#default_value' => $preset->name,
  );


  // Options Vertical Tab Group table
  $form['preset'] = array(
    '#type' => 'vertical_tabs',
  );

  $default_options = workflow_hide_fields_option_elements($preset->options);
  // Add the options to the vertical tabs section
  foreach ($default_options as $key => $value) {
    $form['options'][] = $value;
  }

  if (!empty($form_state['values']['nodeType'])) {
    $selected = $form_state['values']['nodeType'];

    // When the form is rebuilt during ajax processing, the $selected variable
    // will now have the new value and so the options will change.    
    $completeListOfFields = _ajax_get_fields_options($selected);
  }
  else {
    $completeListOfFields = _ajax_get_fields_options($form['options'][0]['nodeType']['#default_value']);
  }
  
  $fieldResponsible['_select_an_item'] = t('Please select an item');
  $fieldResponsible = array_merge($fieldResponsible, $completeListOfFields);
  $form['options'][0]['fields']['fieldResponsible']['#options'] = $fieldResponsible;
  $fieldRating['_select_an_item'] = t('Please select an item');
  $fieldRating = array_merge($fieldRating, $completeListOfFields);
  $form['options'][0]['fields']['fieldRating']['#options'] = $fieldRating;
  $fieldStates['_select_an_item'] = t('Please select an item');
  $fieldStates = array_merge($fieldStates, _get_fields_of_workflow_type($completeListOfFields));
  $form['options'][0]['fields']['fieldState']['#options'] = $fieldStates;
  $form['options'][0]['fields']['fieldsWritableOnlyOnCreation']['#options'] = $completeListOfFields;

  if (!empty($form_state['values']['fieldState'])) {
    $selected = $form_state['values']['fieldState'];
    // When the form is rebuilt during ajax processing, the $selected variable
    // will now have the new value and so the options will change.    
    $listOfStates = _ajax_get_states($selected);
  }
  else {
    $listOfStates = _ajax_get_states($form['options'][0]['fields']['fieldState']['#default_value']);
  }

  // Remove the "creation" state
  reset($listOfStates);
  unset($listOfStates[key($listOfStates)]);

  $form['options'][0]['states']['stateResponsibleAssignment']['#options'] = $listOfStates;
  $form['options'][0]['states']['statesFinal']['#options'] = $listOfStates;
  $form['options'][0]['states']['statesRating']['#options'] = $listOfStates;
  $form['options'][0]['states']['statesResponsibleAssigned']['#options'] = $listOfStates;

  return $form;
}

/**
 * Global settings form builder.
 * 
 * @param array $form
 *    An associative array containing the structure of the form.
 * @param array $form_state
 *   A keyed array containing the current state of the form.
 * @return array
 *   The form with the global settings fields.
 */
function workflow_hide_fields_settings_form($form, $form_state) {
  // TODO: Think if this form is useful for something, otherwise remove it.
  $settings = variable_get('workflow_hide_fields_settings', array('checkbox' => TRUE)); //, 'label_prefix_value' => t('Workflow')));

  $form['settings'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );

  $form['settings']['checkbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check it?'),
    '#description' => t('Just a checkbox.'),
    '#default_value' => $settings['checkbox'],
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}
